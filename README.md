# Grading

A collection of grading scripts used in my employment at the @UCSC (UC Santa
  Cruz) Computer Science Department.

### Positions Held

| Quarter     | Course       | Position(s) | Instructor |
|-------------|--------------|-------------|------------|
| Spring 2015 | **CMPS 12B** | TA/Grader   | Tantalo    |
| Fall 2015   | **CMPS 5J**  | TA/Grader   | Tantalo    |
| Fall 2015   | **CMPS 12B** | TA          | Whitehead  |
| Winter 2016 | **CMPS 12A** | TA/Grader   | Tantalo    |
| Spring 2016 | **CMPS 12B** | TA/Grader   | Tantalo    |
| Spring 2016 | **CMPS 102** | Grader      | Warmuth    |
| Summer 2016 | **CMPS 12B** | Grader      | Tantalo    |
| Fall 2016   | **CMPS 101** | TA/Grader   | Tantalo    |
| Winter 2017 | **CMPS 12A** | TA/Grader   | Tantalo    |
| Spring 2017 | **CMPS 11**  | TA/Grader   | Tantalo    |

#### Course Descriptions

| Course                                                                                                  | Description                                                                                                                                           |
|---------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------|
| [Introduction to Programming in Java (**CMPS 5J**)](https://courses.soe.ucsc.edu/courses/cmps5j)      | Programming in a Java fork (Processing) for students with no prior programming knowledge.                                                           |
| [Intermediate Programming (**CMPS 11**)](https://courses.soe.ucsc.edu/courses/cmps11)                 | Introduction to the Unix shell and programming and compiling in Java.                                                                                 |
| [Introduction to Programming (**CMPS 12A**)](https://courses.soe.ucsc.edu/courses/cmps12a)            | Equivalent to courses 5J and 11 combined.                                                                                                             |
| [Introduction to Data Structures (**CMPS 12B**)](https://courses.soe.ucsc.edu/courses/cmps12b)        | Teaches various data structures, sorting algorithms, and big O notation alongside Java programming and Unix.                                          |
| [Algorithms and Abstract Data Types (**CMPS 101**)](https://courses.soe.ucsc.edu/courses/cmps101)     | Mathematical proofs and implementation of many abstract data types.                                                                                   |
| [Introduction to Analysis of Algorithms (**CMPS 102**)](https://courses.soe.ucsc.edu/courses/cmps102) | Mathematical proofs and solving problems using 4 general categories of algorithms: divide and conquer, greedy, dynamic programming, and network flow. |

#### Position Descriptions

| Position                | Description                                                                                                                                                                                          |
|-------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Course Tutor (**TA**) | Held lab sections of up to 30 students, lecturing and answering questions on topics gone over in class, assisted students in completing programming assignments                                      |
| Reader (**Grader**)   | Graded lab assignments and programs on the Unix timeshare manually and using self developed, partially automated shell scripts. Graded written homework assignments (proofs), midterms, and exams. |
